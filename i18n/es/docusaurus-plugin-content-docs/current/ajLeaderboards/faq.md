---
id: faq
title: Preguntas frecuentes
sidebar_label: Preguntas frecuentes
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

Estas son algunas de las preguntas más frecuentes y sus respuestas. Puede utilizar la barra de la derecha para saltar a su pregunta.

---

## Nadie aparece en la clasificación (`---`)

Este problema es probablemente una combinación de las dos siguientes preguntas frecuentes. Léalas para saber cómo solucionar este problema.

## Los administradores no aparecen en la clasificación

Esto se debe probablemente a que tienen el permiso [`ajleaderboards.dontupdate.<board>`](setup/permissions#ajleaderboards.dontupdate.<board>). Este permiso se da a los OPs y a la gente con el permiso `*` por defecto.

Para solucionarlo, niega el permiso en tu plugin de permisos o deshabilítalo.

Por ejemplo, para negar el permiso si estás usando LuckPerms, usa este comando:

```
/luckperms user <nombredeusuario> permission set ajleaderboards.dontupdate.* false
```

También puedes desactivar el permiso desactivando `enable-dontupdate-permission` en la configuración.

## ¿Por qué la tabla de clasificación sólo muestra los jugadores en línea?
**ajLeaderboards mostrará a los jugadores desconectados**

ajLeaderboards sólo añade jugadores a la clasificación una vez que se han unido al menos una vez desde que se creó la clasificación. Eso significa que los jugadores tendrán que unirse una vez después de configurar la tabla de clasificación para aparecer en ella. Una vez añadidos a la clasificación, permanecerán en ella (incluso si se van) a menos que los elimines manualmente.

Puedes intentar añadir jugadores manualmente usando `/ajlb updateplayer <board> <player>`, pero no todos los marcadores de posición lo soportan. Si no funciona, deberías pedir al desarrollador del plugin que tiene el marcador de posición que añada soporte para marcadores de posición de jugadores offline.

Si el comando `updateplayer` es capaz de actualizar jugadores offline, puedes intentar actualizar todos los jugadores que se hayan unido al servidor usando `/ajlb updatealloffline <board>`.

## Estoy tratando de mostrar la tabla de clasificación, pero sólo dice `Board does not exist`.
Asegúrese de que ha escrito correctamente el nombre del tablero. Si está creando un cartel, debería autocompletarse en el comando, así que vuelva a comprobarlo.
También puedes comprobar `/ajlb list` y asegurarte de que el tablero aparece en la lista.

Asegúrate también de que no has puesto `%` alrededor del nombre del tablero `<tablero>`. Por ejemplo, en lugar de `%statistic_player_kills%` debería poner `statistic_player_kills`.

Si el marcador de posición que estás intentando usar no aparece en ninguno de los dos, por favor asegúrate de que has seguido la [guía de configuración](/ajleaderboards/setup)

## ¿Cómo puedo mostrar el prefijo / rango del jugador en la tabla de clasificación?
ajLeaderboards puede mostrar el prefijo del jugador en la tabla de clasificación incluso cuando están desconectados.

Asegúrate de que tienes [vault](https://www.spigotmc.org/resources/vault.34315/) instalado en tu servidor,
entonces puedes poner el [marcador de prefijo ajLeaderboards](/ajleaderboards/setup/placeholders#ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_prefix)
delante del marcador de nombre para mostrar su prefijo.

## ¡Los marcadores de posición no funcionan con hologramas!
Si estás usando HolographicDisplays, te recomiendo que cambies a [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/).
HolographicDisplays ha hecho algunos cambios que hacen que sea mucho más confuso utilizar marcadores de posición sin ninguna buena razón.
Por ello, te recomiendo que lo dejes.
Como se ha mencionado anteriormente en este párrafo, recomiendo DecentHolograms, pero cualquier plugin de hologramas compatible con PlaceholderAPI funcionará (que ya no considero que HolographicDisplays tenga compatibilidad total con PlaceholderAPI).

Este cambio de HolographicDisplays no tiene sentido para mí, ya que implementar el soporte adecuado de PlaceholderAPI es *una línea de código*.

Mira cómo convertir hologramas HolographicDisplays a DecentHolograms [aquí](https://wiki.decentholograms.eu/general/compatibility#holographicdisplays)


## ¡Mi servidor se retrasa con ajLeaderboards!

Lo primero que debes intentar es desactivar (poner a `false` o `auto`) `blocking-fetch` en la configuración, luego usa `/ajlb reload`.

Si tu servidor sigue teniendo lag, haz un informe de tiempos y envíamelo a discord (el enlace de invitación está en la página del plugin).

## Todas las posiciones en la tabla de clasificación tienen el mismo valor (pero diferentes jugadores)
Asegúrate de leer donde dice ⚠️Important⚠️ en [paso 1 en la guía de configuración](/ajleaderboards/setup#1-required-figure-out-which-placeholder-to-use)

## ¿Cómo puedo dar recompensas?

Actualmente, la única manera de dar recompensas es usando [luckperms contexts](/ajleaderboards/setup/luckperms-contexts) para dar permisos/prefijos/sufijos cuando un jugador está en una cierta posición en la tabla de clasificación

## ¡Los jugadores no se actualizan!

ajLeaderboards tiene una herramienta de diagnóstico incorporada para ver por qué un determinado jugador puede no estar actualizado.

Ejecuta `/ajlb checkupdate <tablero> <jugador>` y mira lo que dice.

Si dice que ese jugador debería actualizarse en el tablero, asegúrese primero de que aparece el mismo valor en `/ajlb list <board>`.

Si el jugador tiene el mismo valor en la tabla de clasificación dada por el comando list, entonces el marcador de posición podría no estar devolviendo un número.
Para comprobarlo, ejecute `/papi parse <jugador> %placeholder%`, donde `<jugador>` es el jugador que no se está actualizando,
y `%placeholder%` es el marcador de posición objetivo (el que pusiste en el comando ajlb add). Para que ajLeaderboards lo analice, debe mostrar un número (en blanco)