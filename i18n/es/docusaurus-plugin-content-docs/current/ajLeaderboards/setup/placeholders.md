---
id: placeholders
title: Marcadores de posición (Placeholders)
sidebar_label: Marcadores de posición
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

import APITable from '/src/components/APITable';

Estos marcadores de posición son para la salida de tablas de clasificación.
¡No intentes usarlos en el comando ajLeaderboards add!

Si no sabes como usarlos, revisa [el ejemplo sobre hologramas en la página de configuración](/ajleaderboards/setup#3-opcional-mostrar-la-clasificación-en-un-holograma).

**No intentes usarlos sin antes [configurar el plugin](/ajleaderboards/setup).**

**__No olvides rellenar__** `<board>` y `<number>` en los marcadores de posición. No funcionarán sin ellos.
`<board>` es el nombre de tu tabla de clasificación (por ejemplo, `statistic_player_kills`), y
`<number>` es la posición que quieres mostrar (1 para el 1º, 2 para el 2º, etc)
`<type>` es el tipo de tiempo (por ejemplo, `alltime`, `hourly`, `daily`, `weekly`, `monthly`).


Ten en cuenta que esta lista sólo se aplica a la última versión de ajLeaderboards. Las versiones anteriores pueden carecer de algunos (o todos) de estos marcadores de posición.

## Marcadores de posición
Marcadores de posición que se utilizan para mostrar una tabla de clasificación

<APITable>

| Placeholder                                         | Description                                                                                                                                              |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_lb_<board>_<number>_<type>_name%`            | Muestra el nombre del jugador en el lugar `<number>` de la tabla de clasificación `<board>`.                                                             |
| `%ajlb_lb_<board>_<number>_<type>_displayname%`     | Muestra el nombre del jugador que ocupa el lugar `<number>` en la tabla de clasificación `<board>`.                                                      |
| `%ajlb_lb_<board>_<number>_<type>_value%`           | Muestra la puntuación del jugador en el lugar `<number>` de la tabla de clasificación `<board>`.                                                         |
| `%ajlb_lb_<board>_<number>_<type>_rawvalue%`        | Muestra la puntuación del jugador en el lugar `<number>` de la tabla de clasificación `<board>`, pero sin comas                                          |
| `%ajlb_lb_<board>_<number>_<type>_value_formatted%` | Muestra la puntuación formateada del jugador en el lugar `<number>` de la tabla de clasificación `<board>`. Ejemplo: 10000 se muestra como 10k           |
| `%ajlb_lb_<board>_<number>_<type>_prefix%`          | Muestra el prefijo de la bóveda del jugador en `<number>` lugar en la tabla de clasificación `<board>`                                                   |
| `%ajlb_lb_<board>_<number>_<type>_suffix%`          | Muestra el sufijo de la bóveda del jugador en `<number>` lugar en la tabla de clasificación `<board>`                                                    |
| `%ajlb_lb_<board>_<number>_<type>_color%`           | Muestra el color del nombre del jugador. Es básicamente el prefijo pero sólo los códigos de color.                                                       |
| `%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`   | Muestra el marcador de posición extra para el jugador en esa posición en la tabla de clasificación. `<extra>` es el marcador de posición extra (sin `%`) |

</APITable>


## Marcadores de posición de jugador
Marcadores de posición que muestran el jugador que está viendo el marcador de posición (cada jugador verá lo suyo)

<APITable>

| Placeholder                             | Description                                                                                                                |
|-----------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_position_<board>_<type>%`        | Muestra en qué posición se encuentra el jugador en la tabla de clasificación `<board>`.                                    |
| `%ajlb_value_<board>_<type>%`           | Muestra el valor del usuario para `<board>`.                                                                               |
| `%ajlb_value_<board>_<type>_formatted%` | Muestra el valor del usuario para `<board>`, y formateado                                                                  |
| `%ajlb_value_<board>_<type>_raw%`       | Muestra el valor bruto del usuario para `<board>`. Sin ningún tipo de formato. (incluyendo comas)                          |
| `%ajlb_extra_<extra>%`                  | Muestra la salida del reproductor para el marcador de posición extra. `<extra>` es el marcador de posición extra (sin `%`) |

</APITable>

## Otros marcadores de posición

<APITable>

| Placeholder                                         | Description                                                                                                                      |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_reset_<type>%`                               | Displays the time until the specified timed type is supposed to reset.    |
| `%ajlb_size_<board>%`                               | Shows the number of players on the specified board. Requires that the position placeholder be used somewhere.    |

</APITable>


## Marcadores de posición relativos
Marcadores de posición que se utilizan para mostrar a los jugadores alrededor del jugador que ve el marcador de posición.

`<relative>` es lo relativo a la posición del jugador que debe ser. Por ejemplo, `+1` habría una posición por encima. `-1` sería una posición por debajo.

<APITable>

| Placeholder                                            | Description                                                                                                                                              |
|--------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_rel_<board>_<type>_<relative>_name%`            | Muestra el nombre del jugador en el lugar `<number>` de la tabla de clasificación `<board>`.                                                             |
| `%ajlb_rel_<board>_<type>_<relative>_displayname%`     | Muestra el nombre del jugador que ocupa el lugar `<number>` en la tabla de clasificación `<board>`.                                                      |
| `%ajlb_rel_<board>_<type>_<relative>_value%`           | Muestra la puntuación del jugador en el lugar `<number>` de la tabla de clasificación `<board>`.                                                         |
| `%ajlb_rel_<board>_<type>_<relative>_rawvalue%`        | Muestra la puntuación del jugador en el lugar `<number>` de la tabla de clasificación `<board>`, pero sin comas                                          |
| `%ajlb_rel_<board>_<type>_<relative>_value_formatted%` | Muestra la puntuación formateada del jugador en el lugar `<number>` de la tabla de clasificación `<board>`. Ejemplo: 10000 se muestra como 10k           |
| `%ajlb_rel_<board>_<type>_<relative>_prefix%`          | Muestra el prefijo de la bóveda del jugador en `<number>` lugar en la tabla de clasificación `<board>`                                                   |
| `%ajlb_rel_<board>_<type>_<relative>_suffix%`          | Muestra el sufijo de la bóveda del jugador en `<number>` lugar en la tabla de clasificación `<board>`                                                    |
| `%ajlb_rel_<board>_<type>_<relative>_color%`           | Muestra el color del nombre del jugador. Es básicamente el prefijo pero sólo los códigos de color.                                                       |
| `%ajlb_rel_<board>_<type>_<relative>_extra_<extra>%`   | Muestra el marcador de posición extra para el jugador en esa posición en la tabla de clasificación. `<extra>` es el marcador de posición extra (sin `%`) |
| `%ajlb_rel_<board>_<type>_<relative>_position%`        | Muestra la posición del jugador relativa                                                                                                                 |

</APITable>