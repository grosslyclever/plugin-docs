---
id: extras
title: Extras
sidebar_label: Extras
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

Desde ajLeaderboards v2.5.0, puedes usar extras para mostrar cosas junto a los jugadores en la tabla de clasificación,
o para mostrar algo como una tabla de clasificación que normalmente no podría ser una tabla de clasificación (por ejemplo, algo que es texto en lugar de un número).

Los extras no tienen por qué ser un número. Pueden ser cualquier tipo de marcador de posición.

Por ejemplo, puedes usar extras para mostrar los asesinatos y muertes de un jugador junto a su kdr,
usarlo para mostrar algo que no sea un prefijo/sufijo junto al nombre del jugador, o usarlo para mostrar formato personalizado del plugin fuente.

## Configuración

Igual que necesitas añadir un marcador de posición usando `/ajlb add` para ajLeaderboards para empezar a rastrearlo, necesitas hacer
algo similar para los extras.

En lugar de hacer un comando para añadir un extra, necesitas añadirlo a la configuración.
Añádelo a la opción de configuración llamada `extras` (asegúrate de leer el comentario sobre esa opción)
luego usa `/ajlb reload` y ajLeaderboards comenzará a rastrearlo.

Al igual que al añadir una tabla de clasificación, los jugadores que no hayan estado conectados desde que añadiste el extra puede que no tengan un extra (se mostraría como "extra").
tener un extra (mostraría `---`)

## Visualización junto a una clasificación existente

Para mostrar un extra junto a una tabla de clasificación existente, debe utilizar el marcador de posición de tabla de clasificación extra.

Asegúrate de seguir la configuración anterior.

Desde [la página de marcadores de posición](/ajleaderboards/setup/placeholders#%ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_extra_%3Cextra%3E%):
`%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`

Simplemente añade este marcador de posición al principio/final (donde quieras mostrarlo) de tu tabla de clasificación existente

## Mostrar formato personalizado

Para mostrar el formato personalizado de un extra en lugar del valor normal de ajLeaderboards, debe utilizar el marcador de posición extra leaderboard.

Aún tendrás que
[configurar una tabla de clasificación ajLeaderboards con un número normal](/ajleaderboards/setup),
pero cuando vayas a mostrarla, en lugar de usar el marcador de posición `value`, usarás el marcador de posición extra.

De [la página de marcadores de posición](/ajleaderboards/setup/placeholders#%ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_extra_%3Cextra%3E%):
`%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`


## Conclusión

Si tienes alguna pregunta, asegúrate de haber leído esta página completa primero, luego puedes unirte a mi discord y hacerme preguntas (el enlace de invitación está en la página del plugin).
Desafortunadamente, sólo puedo proporcionar apoyo en Inglés.
