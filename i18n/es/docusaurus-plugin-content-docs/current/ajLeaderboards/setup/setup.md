---
id: setup
title: Configurar
sidebar_label: Configurar
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

Aquí están los pasos para configurar el plugin. Si tienes alguna pregunta, puedes contactar conmigo (preferiblemente en discord. Enlace de invitación en la página del plugin)

Asegúrate de tener el permiso `ajleaderboards.use` para configurar el plugin.

## 1. (OBLIGATORIO) Averiguar qué marcador de posición usar
ajLeaderboards rastrea los tops mirando lo que un jugador vería si viera un determinado marcador de posición desde un plugin.
Debido a esto, ajLeaderboards soporta miles de plugins sin trabajo extra para ninguno de los desarrolladores.

Puedes encontrar una lista de marcadores de posición [aquí](https://placeholderapi.com/placeholders).
Esta no es una lista completa, pero muestra todos los marcadores de posición papi incorporados y algunos añadidos por plugins también.

El marcador de posición que elijas debe devolver sólo un número normal. El número puede tener comas, pero no puede ser formateado, y no puede tener texto.

:::caution importante

Cuando elija un marcador de posición, asegúrese de que no es ya un marcador de posición superior de ese plugin.
Una forma fácil de saberlo es si el marcador de posición dice "leaderboard" o "top".
Los marcadores de posición top existentes no funcionarán con ajLeaderboards.

<details><summary>¿Por qué no puedo usar marcadores de posición top existentes en ajLeaderboards?</summary>
ajLeaderboards está diseñado para hacer tablas de clasificación de plugins que no tienen una tabla de clasificación incorporada. Si un plugin ya tiene una tabla de clasificación integrada, puedes usarla.

ajLeaderboards funciona analizando un marcador de posición como un jugador (por ejemplo, `%vault_eco_balance%` para obtener el dinero del jugador), y luego almacenando ese valor. Los valores son entonces ordenados para ser mostrados en una tabla de clasificación.
</details>

Algunos ejemplos de marcadores de posición utilizables son `%vault_eco_balance%`, `%statistic_player_kills%`, `%buildbattle_wins%`, `%statistic_time_played%`, `%villagedefense_kills%`, `%ajpk_stats_highscore%`, y `%thebridge_points%`.

:::

En esta guía, voy a utilizar el marcador de posición `%statistic_player_kills%` como ejemplo, que al final creará una tabla de clasificación de muertes.

## 2. (OBLIGATORIO) Añade el marcador de posición a ajLeaderboards

Una vez que hayas elegido un marcador de posición para usar, necesitas registrar el marcador de posición con ajLeaderboards para que podamos empezar a registrar puntuaciones para la tabla de clasificación.

Para agregar un marcador de posición, usa el comando `/ajlb add %placeholder%`, y asegúrate de reemplazar `%placeholder%` con el marcador de posición que elegiste en el paso uno.

Usando el ejemplo de los asesinatos, yo usaría el comando `/ajlb add %statistic_player_kills%`.

Ahora que has añadido el marcador de posición, ten en cuenta que `<board>`, o el nombre del tablero, es lo que pones en el comando add (pero sin %)

**NOTA:** cualquiera con el permiso [`ajleaderboards.dontupdate.<board>`](permissions#ajleaderboards.dontupdate.<board>)
no será añadido a la tabla de clasificación. [Más información](/ajleaderboards/faq#los-administradores-no-aparecen-en-la-clasificación)

## 3. (opcional) Mostrar la clasificación en un holograma

Si lo deseas, puedes mostrar la clasificación en un holograma.

Para mostrar la tabla de posiciones en un holograma, simplemente usa el [PAPI placeholders](/ajleaderboards/setup/placeholders).

Puedes usar cualquier plugin de holograma que soporte placeholdersAPI. Te recomiendo que utilices [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/)

Aquí hay un ejemplo con MurderMystery __alltime__ gana.

En este ejemplo, cada línea es una línea en el holograma.
```
&6=== &cMurderMystery&e top wins &6===
&61. &e%ajlb_lb_murdermystery_wins_1_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_1_alltime_value%
&62. &e%ajlb_lb_murdermystery_wins_2_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_2_alltime_value%
&63. &e%ajlb_lb_murdermystery_wins_3_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_3_alltime_value%
&64. &e%ajlb_lb_murdermystery_wins_4_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_4_alltime_value%
&65. &e%ajlb_lb_murdermystery_wins_5_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_5_alltime_value%
&66. &e%ajlb_lb_murdermystery_wins_6_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_6_alltime_value%
&67. &e%ajlb_lb_murdermystery_wins_7_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_7_alltime_value%
&68. &e%ajlb_lb_murdermystery_wins_8_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_8_alltime_value%
&69. &e%ajlb_lb_murdermystery_wins_9_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_9_alltime_value%
&610. &e%ajlb_lb_murdermystery_wins_10_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_10_alltime_value%
```

Este es el aspecto que tendrá el holograma:
![Ejemplo de holograma](/img/hologram_example.png)

Si los marcadores de posición no te funcionan (sólo aparecen los marcadores de posición), asegúrate de que tu plugin de hologramas admite marcadores de posición PlaceholderAPI.
Si no es así, te recomiendo que cambies a [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/).

Si tienes cualquier otro problema, asegúrate de leer las [FAQ](/ajleaderboards/faq) antes de acudir al servicio de asistencia.

## 4. (opcional) Mostrar la clasificación mediante signos

También puede mostrar la tabla de clasificación utilizando signos.

Para hacer carteles de clasificación, simplemente coloque carteles en blanco donde quiera que estén. (cada signo es una persona)

Para añadir las señales, utilice el comando `/ajlb signs add <board> <position> <type>`.

Por ejemplo, con la tabla de clasificación de ejemplo de antes (statistic_player_kills),
si quisieras mostrar al jugador en la posición #1 de todos los tiempos, mirarías el signo y usarías este comando:
`/ajlb signs add statistic_player_kills 1 alltime`

## 5. (opcional) Mostrar la tabla de clasificación usando cabezas

Si quieres mostrar la clasificación usando cabezas, puedes hacerlo.

Sólo tienes que seguir el paso 4 y colocar una cabeza encima o delante del cartel.

## 6. (opcional) Mostrar la tabla de clasificación usando soportes de armadura

Si quieres mostrar la tabla de clasificación usando una cabeza en un soporte de armadura, puedes hacerlo.

Sólo tienes que seguir el paso 4, a continuación, coloque una armadura de pie por encima o delante de la señal

## 7. (opcional) Mostrar la tabla de clasificación usando NPCs

Si quieres mostrar la leaderbaords usando ncps, puedes usar [Citizens](https://www.spigotmc.org/resources/citizens.13811/).

Asegúrate de que estás ejecutando una versión reciente de Citizens.

Luego, simplemente crea un npc. Establece el skin como el marcador de posición ajLeaderboards, y el nombre como el marcador de posición name y el valor. Mira el ejemplo de abajo.

Si quieres cambiar la frecuencia de actualización de los npcs, puedes hacerlo con la opción `placeholder-update-frequency-ticks` en la configuración de Citizen.

### Ejemplo
En este ejemplo, voy a crear un NPC que mostrará el jugador # 1 para el `statistic_player_kills` alltime leaderboard.

1. Crea el npc con el nombre y el valor. Este comando hará que el nombre se muestre así: `ajgeiss0702 - 12 muertes`
```
/npc create &a%ajlb_lb_statistic_player_kills_1_alltime_name% &7- &6%ajlb_lb_statistic_player_kills_1_alltime_value% muertes
```
2. Establecer el marcador de posición de la piel.
```
/npc skin %ajlb_lb_statistic_player_kills_1_alltime_skin%
```
## 8. (opcional) Crear tablas de clasificación por tiempo

Además de las tablas de clasificación normales de todos los tiempos (`alltime`), también puedes crear tablas de clasificación que se reinicien automáticamente cada hora, cada día, cada semana, cada mes y cada año.
:::caution note

Antes de crear una tabla de clasificación cronometrada, ten en cuenta que la tabla de clasificación cronometrada sólo funciona para ciertos tipos de estadísticas de marcador de posición.

Sólo puedes hacer tablas de clasificación cronometradas para las estadísticas que son acumulativas (se suman con el tiempo).

For example, you **can** make a timed leaderboard for total number of kills, total number of votes, etc. <br/>
Usted **no puede** hacer una tabla de clasificación cronometrada para cosas como una puntuación alta.

Todavía puedes hacer una tabla de clasificación para estadísticas no acumulativas, pero sólo debes usar `alltime`. Los otros tipos de tiempo no funcionarán correctamente.

:::

Para crear una tabla de clasificación cronometrada, primero debes seguir los pasos 1 y 2 de la misma manera que lo harías si estuvieras creando una tabla de clasificación de todos los tiempos.
Si ya has creado una tabla de clasificación de todos los tiempos, no necesitas seguir los pasos de nuevo.

Una vez que haya agregado el marcador de posición a ajLeaderboards, para mostrar las tablas de clasificación cronometradas, simplemente use las pantallas `alltime`, pero reemplace todos los `alltime`s con el tipo correcto.

Los tipos válidos son
`alltime`,
`hourly`,
`daily`,
`weekly`,
`monthly`, and
`yearly`

Por ejemplo, si quisieras cambiar el ejemplo del holograma anterior de todo el tiempo a mensual, quedaría así:
```
&61. &e%ajlb_lb_murdermystery_wins_1_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_1_monthly_value%
&62. &e%ajlb_lb_murdermystery_wins_2_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_2_monthly_value%
&63. &e%ajlb_lb_murdermystery_wins_3_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_3_monthly_value%
&64. &e%ajlb_lb_murdermystery_wins_4_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_4_monthly_value%
&65. &e%ajlb_lb_murdermystery_wins_5_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_5_monthly_value%
&66. &e%ajlb_lb_murdermystery_wins_6_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_6_monthly_value%
&67. &e%ajlb_lb_murdermystery_wins_7_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_7_monthly_value%
&68. &e%ajlb_lb_murdermystery_wins_8_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_8_monthly_value%
&69. &e%ajlb_lb_murdermystery_wins_9_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_9_monthly_value%
&610. &e%ajlb_lb_murdermystery_wins_10_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_10_monthly_value%
```
Fíjese cómo se ha cambiado el `<type>` del marcador de posición de `alltime` a `monthly`.
