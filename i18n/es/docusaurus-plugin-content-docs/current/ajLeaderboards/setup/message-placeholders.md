---
id: message-placeholders
title: Marcadores de posición de mensajes
sidebar_label: Marcadores de posición de mensajes
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

import APITable from '/src/components/APITable';

Marcadores de posición utilizados en los mensajes

### Signs

<APITable>

| Placeholder   | Description                                                 |
|---------------|-------------------------------------------------------------|
| `{POSITION}`  | La posición en la tabla de clasificación                             |  
| `{NAME}`      | El nombre del jugador                                      |
| `{VALUE}`     | La puntuación/valor del jugador                               |
| `{FVALUE}`    | Puntuación/valor del jugador (formateado)                   |
| `{TVALUE}`    | La puntuación/valor del jugador (formateada como tiempo)         |
| `{VALUENAME}` | El "nombre-valor" del tablero (`value-names` en el config) |
| `{TIMEDTYPE}` | El TimedType de la señal (por ejemplo, `alltime`, `hourly`, etc)       |

</APITable>