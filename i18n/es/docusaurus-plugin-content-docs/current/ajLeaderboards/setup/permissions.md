---
id: permissions
title: Permisos
sidebar_label: Permisos
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

import APITable from '/src/components/APITable';

Esta es una lista de permisos para el plugin.

<APITable>

| Permission                          | Description                                                                       |
|-------------------------------------|-----------------------------------------------------------------------------------|
| `ajleaderboards.use`                | Permiso para usar comandos en /ajleaderboards                                     |
| `ajleaderboards.dontupdate.<board>` | Los jugadores con este permiso no serán actualizados en la tabla de clasificación |

</APITable>