---
id: luckperms-contexts
title: Contextos LuckPerms
sidebar_label: Contextos LuckPerms
---
:::caution Precaución
Esta página se ha traducido automáticamente. Es posible que algunas traducciones no sean del todo correctas,
aunque deberían aproximarse bastante ya que el traductor utilizado es decente.<br/>
Se aceptan modificaciones. Haga clic en el enlace de edición al final de la página y envíe una solicitud de fusión.
:::

Desde ajLeaderboards v2.5.2, puedes usar contextos [LuckPerms](https://luckperms.net/) para dar
permisos/prefijos/sufijos a los jugadores que están en ciertas posiciones en la tabla de clasificación.

Asegúrate de habilitarlos en la opción de configuración `register-lp-contexts`.
Recomiendo encarecidamente configurar también `only-register-lpc-for` para registrar sólo los contextos que vayas a utilizar realmente.

### Explicación

Si no conoces los contextos LuckPerms, puedes leer sobre ellos [aquí](https://luckperms.net/wiki/Context).

ajLeaderboards añade contextos personalizados (dinámicos) que permiten comprobar la posición de un jugador.

El formato del contexto es `ajlb_pos_<board>_<type>` donde `<board>` es el nombre del tablero, y `<type>` es el Tipo Temporizado

### Ejemplo

Por ejemplo, si quieres dar permiso para el comando essentials back al jugador que
es **#1** en la tabla de clasificación `static_time_played` `alltime`, entonces podrías usar este comando:
```
/luckperms group default permission set essentials.back true ajlb_pos_statistic_time_played_alltime=1
```
Esto les dará el permiso mientras sean #1 en la tabla de clasificación (y les quitará el permiso si pierden ese puesto)


