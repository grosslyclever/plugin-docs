---
id: overview
title: ajLeaderboards
sidebar_label: Visión general
slug: /ajleaderboards/
---

ajLeaderboards es un plugin gratuito para crear tablas de clasificación basadas en marcadores de posición PlaceholderAPI en tu servidor.

Si quieres configurar el plugin, dirígete a la [página de configuración](/ajleaderboards/setup).

[Spigot](https://www.spigotmc.org/resources/ajleaderboards.85548/) [Modrinth](https://modrinth.com/plugin/ajleaderboards) [Polymart](https://polymart.org/resource/ajleaderboards.2726)
