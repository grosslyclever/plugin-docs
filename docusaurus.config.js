var path = require('path');
module.exports = {
  title: "aj's Plugins Wiki",
  tagline: 'Find useful information about aj\'s plugins',
  url: 'https://wiki.ajg0702.us',
  plugins: [path.resolve(__dirname, 'plugin-arc')],
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'https://ajg0702.us/pics/small_logo.png',
  organizationName: 'ajg0702', // Usually your GitHub org/user name.
  projectName: 'plugin-docs', // Usually your repo name.
  themeConfig: {
    prism: {
      additionalLanguages: ['java', 'kotlin', 'groovy']
    },
    navbar: {
      hideOnScroll: true,
      title: 'aj\'s wiki',
      logo: {
        alt: 'ajgeiss0702',
        src: 'https://ajg0702.us/pics/small_logo.png',
      },
      items: [
        {
          label: 'ajParkour',
          position: 'left',
          to: '/ajparkour/'
        },
        {
          label: 'ajLeaderboards',
          position: 'left',
          to: '/ajleaderboards/'
        },
        {
          label: 'ajQueue',
          position: 'left',
          to: '/ajqueue/'
        },
        {
          label: 'ajTNTRun',
          position: 'left',
          to: '/ajtntrun/'
        },
        {
          label: 'ajStartCommands',
          position: 'left',
          to: '/ajstartcommands/'
        },
        {
          type: 'localeDropdown',
          position: 'right',
        },
        {
          href: '/resources',
          label: "Useful info",
          position: "right"
        },
        {
          href: 'https://about.ajg0702.us/',
          label: 'About aj',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Links',
          items: [
            {
              label: 'Feedbacky',
              href: '/feedbacky',
            },
            {
              label: 'Discord',
              href: 'https://discord.gg/cqdBNbq',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/ajgeiss0702',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/ajg0702',
            },
          ],
        },
        {
          title: 'ㅤ',
          items: [],
        },
        {
          title: 'Attribution',
          items: [
            {
              label: "Some icons from flaticon.com",
              href: "https://flaticon.com"
            }
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} <a style="color: inherit" href="https://about.ajg0702.us/" target="_blank">Aiden Geiss</a>. All rights reserved.`,
    },
    "colorMode": {
      "defaultMode": "dark",
      "disableSwitch": false,
      "respectPrefersColorScheme": true
    },
    docs: {
      sidebar: {
        hideable: true
      }
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarCollapsible: true,
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/ajg0702/plugin-docs/-/edit/master',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
          routeBasePath: "/",
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/ajg0702/plugin-docs/-/edit/master',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'es'],
    localeConfigs: {
      en: {
        htmlLang: 'en-US',
      }
    },
  },
};
