---
title: Queue Scoreboard
sidebar_label: Queue Scoreboard
---
It is possible to show a different scoreboard when the player is in a queue.

This is possible using the Queue Scoreboard Activator Event and a scoreboard plugin that
supports event activators

## Known supported plugins
These plugins are known to support event activators. If theres one that supports it that is missing, let me know.
* [AnimatedScoreboard](https://www.spigotmc.org/resources/animatedscoreboard.20848/) (free)
* [KiteBoard](https://www.spigotmc.org/resources/kiteboard.13694/) (paid and discontinued)

*If you know any other plugins that support event activators, let me know*

## Explanation (how it works)
How this works is that the spigot side of ajQueue emits a bukkit event whenever a player joins the queue.
Some scoreboard plugins are able to pick up on the event, and display a scoreboard.

The event is called every 2 seconds while someone is in the queue.

## Setup
In your scoreboard plugin, find how you make scoreboards based on events, and use the event
`us.ajg0702.queue.spigot.QueueScoreboardActivator`. This event is called every 2 seconds while a player is in the queue,
so if there is a timeout option, set it to 2 or 3 seconds.

## Examples

### AnimatedScoreboard
queuescoreboard.yml in the scoreboards folder:
```yaml
display:
    title:
      text:
      - ""
      random: false
      interval: 20
    line-1:
      text:
      - " "
      random: false
      interval: 20
      score: 15
    line-2:
      text:
      - "&6Welcome, &a&l%player_name%"
      random: false
      interval: 20
      score: 14
    line-3:
      text:
      - " "
      random: false
      interval: 20
      score: 13
    line-4:
      text:
      - "&6Queued for:&f %ajqueue_queued%"
      random: false
      interval: 20
      score: 12
    line-5:
      text:
      - "&7  Position &f%ajqueue_position%&7/&f%ajqueue_of%"
      random: false
      interval: 20
      score: 11
    line-6:
      text:
      - " "
      random: false
      interval: 20
      score: 10
    line-7:
      text:
      - "&6Total players: &a&l%server_online%"
      random: false
      interval: 20
      score: 9
    line-8:
      text:
      - " "
      random: false
      interval: 20
      score: 8
    line-9:
      text:
      - "&e&lyourserver.com"
      random: false
      interval: 20
      score: 7

```
Make sure `enable-triggers` is enabled in the config, then restart the server. <br/>
Then in the triggers folder, make `queue_trigger.yml` with these contents:
```yaml
#The event that will trigger this scoreboard with full package name and classname
event: us.ajg0702.queue.spigot.QueueScoreboardActivator

#It will look for this method when getting the player to target, so if the player method is getPlayer() you put getPlayer
#You are able to find the method in the javadoc of that plugin, most of them will just use getPlayer as method
target-player: getPlayer

#This is the scoreboard that will be displayed when the trigger is called
trigger-scoreboard: queuescoreboard

#For how long will the triggered scoreboard be displayed in ticks. 20 being 1 second
stay-time: 40

#If the event is trigger more than once during the stay time should it reset the entire scoreboard:
# true will reset the entire scoreboard (starting from frame 0 again)
# false will just reset the timer
reset-on-trigger: false

```
Make sure that if you changed the name of the `queuescoreboard.yml` file above, to also change it in `trigger-scoreboard` in `queue_trigger.yml`

### KiteBoard
This is an example of a scoreboard file with KiteBoard.
```yaml
setting:
  priority: 500
  advanced: false

criteria:
  1:
    type: EVENT
    event: "us.ajg0702.queue.spigot.QueueScoreboardActivator"
    event-player: "getPlayer"
    stay-seconds: 3

content:
  - ''
  - '&6Welcome, &a&l%player_name%'
  - ''
  - "20|&6Queued for:&f %ajqueue_queued%"
  - "20|&7  Position &f%ajqueue_position%&7/&f%ajqueue_of%"
  - ''
  - '20|&6Total players: &a&l%server_online%'
  - ''
  - '&e&lyourserver.com'

```
