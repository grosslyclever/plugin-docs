---
id: overview
title: ajQueue
sidebar_label: Overview
slug: /ajqueue/
---

ajQueue is a queue plugin that was made because all of the queue plugins I tried were either very buggy and had a weird config, or were over-priced for what they offered.

[Modrinth (free)](https://modrinth.com/plugin/ajQueue) <br/>
[Polymart (free)](https://polymart.org/resource/ajQueue.2535) <br/>
[Spigot (free)](https://www.spigotmc.org/resources/ajqueue.78266/) <br/>

[Polymart (premium)](https://polymart.org/resource/ajqueueplus.2714) <br/>
[Spigot (premium)](https://www.spigotmc.org/resources/ajqueueplus.79123/) <br/>