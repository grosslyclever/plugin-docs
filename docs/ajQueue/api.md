---
id: api
title: API
sidebar_label: API
---
You are able to hook into ajQueue and add people to the queue directly, list players in queues, etc.

Note that to follow this, you need to be running ajQueue 2.5.0 or newer.

If you need any help with the api, feel free to ask me for help. I prefer discord, but any way to contact me should be fine.

---


# Depending on ajQueue
<details><summary>Maven</summary>

```xml
<repositories>
  <repository>
    <id>ajRepo</id>
    <url>https://repo.ajg0702.us/releases</url>
  </repository>
</repositories>

<dependencies>
  <dependency>
    <groupId>us.ajg0702.queue.api</groupId>
    <artifactId>api</artifactId>
    <version>{ajqueue version}</version>
    <scope>provided</scope>
  </dependency>
</dependencies>
```

</details>

<details><summary>Gradle (Kotlin)</summary>

```kotlin
repositories {
    maven { url = uri("https://repo.ajg0702.us/releases") }
}

dependencies {
    compileOnly("us.ajg0702.queue.api:api:{ajqueue version}")
}
```

</details>


<details><summary>Gradle (Groovy)</summary>

```groovy
repositories {
    maven { url 'https://repo.ajg0702.us/releases' }
}

dependencies {
    compileOnly 'us.ajg0702.queue.api:api:{ajqueue version}'
}
```

</details>



Make sure to replace `{ajqueue version}` with the latest version of ajQueue.

---

## Proxy Side API

The proxy-side is where all the guts of ajQueue is. It is the most direct and feature-complete way to interact with ajQueue.

### Accessing the API

Once you have the dependency in your ide, you can start using the api.

To get an instance of the api, you can use this:

```java
AjQueueAPI.getInstance()
```

You can find the [javadocs for the api class here](https://ajgeiss0702.github.io/ajQueue/us/ajg0702/queue/api/AjQueueAPI.html).

Most of what you would want to do would be under `getQueueManager()`.

Remember, you can only access the API on the proxy side.

### Getting AdaptedPlayer

To get an AdaptedPlayer (which is used in most of ajQueue's api), you use the PlatformMethods class.

You can either use `PlatformMethods#getPlayer(String name)` or `PlatformMethods#getPlayer(UUID uuid)`.

Example:
```java
AdaptedPlayer player = AjQueueAPI.getInstance().getPlatformMethods().getPlayer("ajgeiss0702");
```

Note that the player has to be online.

### Events

Due to ajQueue being cross-platform, it does not use either the BungeeCord or Velocity event system, but uses its own.

This is to ensure that the same code can be used on both platforms.

You can find a [list of events on the javadocs](https://ajgeiss0702.github.io/ajQueue/us/ajg0702/queue/api/events/package-summary.html)

To listen for an event, use the `listen` method in the api.

Here is an example that listens for the [SuccessfulSendEvent](https://ajgeiss0702.github.io/ajQueue/us/ajg0702/queue/api/events/SuccessfulSendEvent.html)

```java
AjQueueAPI.getInstance().listen(SuccessfulSendEvent.class, event -> {
    logger.info(event.getPlayer().getName() + " was sent to " + event.getServer().getName());
});
```

You can also cancel the [PreQueueEvent](https://ajgeiss0702.github.io/ajQueue/us/ajg0702/queue/api/events/PreQueueEvent.html)
which will stop the player from being added to the queue. Remember to tell them *why* you are blocking them from joining the queue!

```java
AjQueueAPI.getInstance().listen(PreQueueEvent.class, event -> {
    if(shouldPreventQueue(event.getPlayer())) {
        event.setCancelled(true);
    }
});
```

If you have ideas for more events I should add, let me know!
The best way to contact me is on discord (invite link is on the plugin page)

---

## Spigot Side API

The spigot side API is mainly just a wrapper for sending plugin messages back to the proxy side, and waiting to get information back.

For this reason, all methods return [Futures](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Future.html)

You can see the [list of methods on the javadocs](https://ajgeiss0702.github.io/ajQueue/us/ajg0702/queue/api/spigot/AjQueueSpigotAPI.html)

All the [spigot-side placeholders](/ajqueue/setup/placeholders) use this API, so anything you can display using the PAPI placeholders, you can also get using the API

### Example
Here is an example of getting whether the player is in a queue.

Remember that `get` is blocking, so make sure not to use it on the main server thread!


```java
Boolean isInQueue = AjQueueSpigotAPI.getInstance()
    .isInQueue(player.getUniqueId())
    .get(5, TimeUnit.SECONDS);
```