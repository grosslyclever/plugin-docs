---
title: Main config
sidebar_label: Main config
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the default main config. Everything is explained in the comments.


<ConfigFetch url="https://raw.githubusercontent.com/ajgeiss0702/ajQueue/master/common/src/main/resources/config.yml"/>
