---
title: Permissions
sidebar_label: Permissions
---

| Permission | Description | Command(s) |
| --- | --- | --- |
| startcommands.reload | Allows you to reload the config | /ajstartcommands reload |
