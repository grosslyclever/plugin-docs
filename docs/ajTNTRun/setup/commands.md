---
title: Commands and permissions
sidebar_label: Commands and permissions
---
Here is a table that lists all commands and their permissions.

| Command | Description | Permission |
| -------: | :-----------: | :---------- |
| /tntrun join | Joins an arena |  |
| /tntrun leave | Leaves an arena |  |
| /tntrun stats | Shows your stats, or the stats of another player |  |
| /tntrun list | Lists all active arenas | |
| /tntrun createarena | Creates an arena and adds it to the edit queue | ajtr.setup |
| /tntrun editing | Lists all arenas in the edit queue | ajtr.setup |
| /tntrun edit | Imports an active arena into the editing queue | ajtr.setup |
| /tntrun reload | Reloads the arena file, messages, and the config | ajtr.reload |
| /tntrun signs | Command for managing arena signs | ajtr.setup |
| /tntrun start | Force-starts an arena that is waiting | ajtr.forcestart |
| | Permission to bypass blocked commands | ajtr.bypasscommands |
| | Permission to join games when they are full| ajtr.joinfull |

## Command aliases
Instead of using `/tntrun`, you can use any of the below commands:
* `/ajtntrun`
* `/atr`
* `/tr`
