---
title: Placeholders
sidebar_label: Placeholders
---
Here is a list of papi placeholders that this plugin provides. They come with the plugin so you do not have to download an ecloud extension for it.


Make sure you are running ajTNTRun v1.0.2 or higher!

| Placeholder | Description |
| ------ | ------ |
|`%ajtr_stats_wins%` | Shows the number of times the player has won a game. |
|`%ajtr_stats_losses%` | Shows the number of times the player has lost a game. |
|`%ajtr_stats_gamesplayed%` | Shows the number of times the player has played a game. |
