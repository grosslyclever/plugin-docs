---
title: Creating an arena
sidebar_label: Creating an arena
---
If you want to watch a video, you can [watch this video](https://www.youtube.com/watch?v=ux1C2WIH-pk). Check the comments for my comment to see some explanations for some things that weren't explained very well in the video.

---

Setting up an arena is fairly simple, and does not require many commands.

Note: the `/tntrun` command can be replaced by any of the [command aliases](/ajtntrun/setup/commands#command-aliases)

### What you should already have
You should have the following prepared:
*  An arena (see the [arena requirements](arena-requirements))
*  An arena lobby (optional)

### Creating the arena
The first step you must take is to create the arena in the editing queue. During this step, you will pick a name for your arena, which you will not be able to change until you have finished setting it up. Keep in mind that the name you chose is what will show up on signs, and is what players will use in the join command.

Use the command `/tntrun createarena Test` will create an arena with the name 'Test'. You can replace Test with whatever you want to name the arena.

### Setting the area
The plugin uses an area shaped like a box that the arena (and its lobby, if you have one) are in. If a player goes outside the box, they will be eliminated.

You define the box by setting opposite corners. One high corner, the other is a low corner. (just like worldedit).

**Note:** These positions are set at the block where your feet are.

To set these positions, use the commands `/tntrun arena Test pos1` and `/tntrun arena Test pos2`, replacing `Test` with the name you picked when using the createarena command.

### Setting the start location
The start location is the place players are teleported to when the game is going to start. (it is also used as a lobby if it is not set.)

To set the start location, stand where the players should be teleported, and type the command `/tntrun arena Test startloc`, replacing `Test` with the name you entered before.

When you do this command, it also records the direction you are looking and will make the players face that direction when it teleports them.

### Setting the lobby position (optional)
The lobby position is where the players are while the arena is waiting for more players. If you do not set a lobby position, the start location will be used as a lobby.

To set the lobby position, stand and look where you would like to set it, and type the command `/tntrun Arena Test lobby`, replacing `Test` with what you named your arena.

### Saving the arena
To allow the arena to be played on, you must save it. Run the command `/tntrun arena Test save`, replacing `Test` with what you named the arena.
If you have set the required positions, it will say that the arena has been saved. The arena is now ready to be joined and played on!

### Setting the main lobby
If this is the first arena you have made, you need to [set the main lobby](setting-a-Lobby) before you are able to play.

###   
To join the arena you just created, use the command `/tntrun join Test`, replacing `Test` with what you named the arena.
