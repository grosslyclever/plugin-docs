---
title: Creating signs
sidebar_label: Creating signs
---
Creating signs is simple. Here are the steps:
1.  Place down a sign.
    *  What you put on the sign does not matter
2.  Put your crosshair on the sign.
3.  Run the command `/tntrun signs add Test`.
    *  Replace Test with the name of the arena you want to put on the sign

That's it! Other commands for managing signs can be seen by doing `/tntrun signs`.
