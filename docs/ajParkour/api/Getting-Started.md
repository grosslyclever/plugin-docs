Note: The API requires ajParkour v2.6.0 or newer!

First, make sure you have imported the ajParkour jar into your IDE. 

The javadocs are available [here](https://ajg0702.gitlab.io/ajparkour/).

# Getting the manager
The manager is the main class you will use to interact with the plugin.

To get the instance of the manager, use this static method:
```java
Manager manager = AjParkour.getManager();
```
From the manager, you can get areas, get players in parkour, start new players in parkour, and more.

To see everything you can do with the manager, check out the [javadoc page](https://ajg0702.gitlab.io/ajparkour/us/ajg0702/parkour/game/Manager.html).