---
id: Home
title: ajParkour
description: ajParkour wiki
sidebar_label: Overview
slug: /ajparkour/
---
![ajp_long.svg](uploads/417262e1f1c2f59922a2dbfb29ceb3da/ajp_long.svg)

To navigate the wiki, use the sidebar on the left.

If you think this wiki is missing something, please tell me on my [discord server](https://discord.gg/FWV457K).

 [Spigot](https://www.spigotmc.org/resources/ajparkour.60909/)
