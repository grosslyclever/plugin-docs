---
id: moving-storage-methods
title: Moving between storage methods
sidebar_label: Moving storage methods
---

This quick step-by-step guide will walk you through how to switch storage methods without losing data.

If you dont care about losing data (timed leaderboards would be reset and players would have to join again to show in all-time),
then simply follow step 2 and restart the server.

### 1. Export
First, we need to save all of the data in the storage method to a file so that we can import it to the new storage method later.

To do this, simply do `/ajlb export <file>`. replace `<file>` with the name for a new file. The name does not matter, as long as you can remember it for later.

This command might take a decent amount of time to complete, depending on how fast the storage method is and how many players are stored.
(for example, if you are on sqlite, it might take several minutes to complete)
It should send progress messages in the chat for each board it finishes

### 2. Switch

Once the export command is done, you can switch the method in the cache storage config file.

Open `cache_storage.yml` and change `method` to your new desired storage method.
See the comment above the `method` option to see the possible values (I recommend `h2`)

Once you've made that change (and saved the file), then restart the server.

### 3. Import
Now you will be on the new storage method, but you will notice that any displays will say `Board does not exist`.
To fix this, we just have to import the data we exported in step 1.

Simply run `/ajlb import <file>` where `<file>` is the same thing you put in the import command.

Note that if it says the file was not found, then you might need to add `.json` to the end.

Again depending on the cache storage and the amount of players stored, this command could take a few minutes.
(although if you are using h2, it should only take a few seconds)

### 4. Done!

You have now successfully moved your data between storage methods!
If you would like, you can delete the old storage file (e.g. `cache.db` for sqlite) and the exported file `<file>.json`.

Make sure to only delete files once you know everything has copied over correctly