---
id: glossary
title: Glossary
sidebar_label: Glossary
---

Generally the definition of these words can be interred by their usage in the docs.
However, in places where it might not be as clear,
you can refer to this page to better understand what certain words or phrases mean.

## Board name

The board name is the same as the placeholder you put in `/ajlb add`, but without `%`.

For example, if you did `/ajlb add %statistic_time_played%`,
you would be creating a board with a board name of `statistic_time_played

## Target placeholder

The target placeholder is the placeholder that ajLeaderboards gets it's data from for a specific board.

For example, for the board `statistic_time_played`, the target placeholder is `%statistic_time_played%`

## Source plugin / Source expansion

The source plugin or source expansion referrs to the plugin or exapnsion that provides the placeholder.

For example, the source expansion for the board `statistic_time_played` is the `statistic` expansion