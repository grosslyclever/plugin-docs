---
id: overview
title: ajLeaderboards
sidebar_label: Overview
slug: /ajleaderboards/
---

ajLeaderboards is a free plugin for creating leaderboards based on placeholderapi placeholders on your server.

If you would like to set up the plugin, head to the [setup page](/ajleaderboards/setup).

[Spigot](https://www.spigotmc.org/resources/ajleaderboards.85548/) [Modrinth](https://modrinth.com/plugin/ajleaderboards) [Polymart](https://polymart.org/resource/ajleaderboards.2726)
