---
id: setup
title: Setup
sidebar_label: Setup
---
Here are the steps for setting up the plugin. If you have any questions, you can contact me (preferably on discord. Invite link on the plugin page)

Make sure you have the permision `ajleaderboards.use` to setup the plugin.

## 1. (REQUIRED) Figure out which placeholder to use
ajLeaderboards tracks tops by looking at what a player would see if they were to view a certain placeholder from a plugin.
Because of this, ajLeaderboards supports thousands of plugins with no extra work for either developers.

You can find a list of placeholders [here](https://placeholderapi.com/placeholders).
This is not a complete list, but it shows all of the built-in papi placeholders and some added by plugins too.

The placeholder you choose must return only a normal number. The number can have commas, but cannot be formatted, and cannot have text.

:::caution important

When picking a placeholder to use, make sure it is not already a top placeholder from that plugin.
An easy way to tell this is if the placeholder says "leaderboard" or "top" in it.
Existing top placeholders will not work with ajLeaderboards.

<details><summary>Why can't I use existing top placeholders in ajLeaderboards?</summary>
ajLeaderboards is meant to make leaderboards out of plugins that dont already have a built-in leaderboard. If a plugin already has a built-in leaderboard, you can just use that.

ajLeaderboards works by parsing a placeholder as a player (e.g. `%vault_eco_balance%` to get the player's money), then storing that value. The values are then sorted to be displayed on a leaderboard.
</details>

Some examples of usable placeholders are `%vault_eco_balance%`, `%statistic_player_kills%`, `%buildbattle_wins%`, `%statistic_time_played%`, `%villagedefense_kills%`, `%ajpk_stats_highscore%`, and `%thebridge_points%`

:::

In this guide, I will be using the placeholder `%statistic_player_kills%` as an example, which in the end will create a kills leaderboard.

## 2. (REQUIRED) Add the placeholder to ajLeaderboards

Once you have chosen a placeholder to use, you need to register the placeholder with ajLeaderboards so that we can start recording scores for the leaderboard.

To add a placeholder, use the command `/ajlb add %placeholder%`, and make sure to replace `%placeholder%` with the placeholder you chose in step one.

Using the kills example, I would do the command `/ajlb add %statistic_player_kills%`.

Now that you've added the placeholder, note that `<board>`, or the board name, is what you put in the add command (but without %)

**NOTE:** anyone with the permission [`ajleaderboards.dontupdate.<board>`](permissions#ajleaderboards.dontupdate.<board>) will not be added to the leaderboard. [More info](/ajleaderboards/faq#admins-dont-show-up-on-the-leaderboard)

## 3. (optional) Display the leaderboard in a hologram

If you would like, you can display the leaderboard in a hologram.

To show the leaderboard in a hologram, simply use the [PAPI placeholders](/ajleaderboards/setup/placeholders).

You can use any hologram plugin that supports PlaceholderAPI placeholders. I recommend that you use [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/)

Here is an example with MurderMystery __alltime__ wins.

In this example, each line is a line in the hologram.
```
&6=== &cMurderMystery&e top wins &6===
&61. &e%ajlb_lb_murdermystery_wins_1_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_1_alltime_value%
&62. &e%ajlb_lb_murdermystery_wins_2_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_2_alltime_value%
&63. &e%ajlb_lb_murdermystery_wins_3_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_3_alltime_value%
&64. &e%ajlb_lb_murdermystery_wins_4_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_4_alltime_value%
&65. &e%ajlb_lb_murdermystery_wins_5_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_5_alltime_value%
&66. &e%ajlb_lb_murdermystery_wins_6_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_6_alltime_value%
&67. &e%ajlb_lb_murdermystery_wins_7_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_7_alltime_value%
&68. &e%ajlb_lb_murdermystery_wins_8_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_8_alltime_value%
&69. &e%ajlb_lb_murdermystery_wins_9_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_9_alltime_value%
&610. &e%ajlb_lb_murdermystery_wins_10_alltime_name% &7- &e%ajlb_lb_murdermystery_wins_10_alltime_value%
```

This is what the hologram will look like:
![Hologram Example](/img/hologram_example.png)

If the placeholders don't work for you (show up as just the placeholders), make sure your hologram plugin supports PlaceholderAPI placeholders.
If it doesnt, I recommend that you switch to [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/)

If there are any other issues, make sure to raad the [FAQ](/ajleaderboards/faq) before going to get support.

## 4. (optional) Display the leaderboard using signs

You can also display the leaderboard using signs.

To make leaderboard signs, simply place blank signs where you want them to be. (each sign is one person)

To add the signs, use the command `/ajlb signs add <board> <position> <type>`

For example, with the example leaderboard from earlier (statistic_player_kills),
if you wanted to show the player in the #1 alltime position, you would look at the sign and use this command:
`/ajlb signs add statistic_player_kills 1 alltime`

## 5. (optional) Display the leaderboard using heads

If you want to show the leaderboard using heads, you can.

Just follow step 4, then place a head above or in front of the sign.

## 6. (optional) Display the leaderboard using armor stands

If you want to show the leaderboard using a head on an armor stands, you can.

Just follow step 4, then place an armor stand above or in front of the sign

## 7. (optional) Display the leaderboard using NPCs

If you want to show the leaderboards using ncps, you can use [Citizens](https://www.spigotmc.org/resources/citizens.13811/).

Make sure you are running a recent version of Citizens.

Then, simply create an npc. Set the skin to be the ajLeaderboards name placeholder, and the name to the name placeholder and the value. See the example below.

If you want to change the rate that the npcs update, you can with the `placeholder-update-frequency-ticks` option in Citizen's config.

### Example
In this example, I will be creating an NPC that will show the #1 player for the  `statistic_player_kills` alltime leaderboard.

1. Create the npc with the name placeholder and the value. This command will have the nametag show like this: `ajgeiss0702 - 12 kills`
```
/npc create &a%ajlb_lb_statistic_player_kills_1_alltime_name% &7- &6%ajlb_lb_statistic_player_kills_1_alltime_value% kills
```
2. Set the skin placeholder.
```
/npc skin %ajlb_lb_statistic_player_kills_1_alltime_skin%
```

## 8. (optional) Create timed leaderboards

As well as normal all-time (`alltime`) leaderboards, you can also create leaderboards that reset automatically every hour, every day, every week, every month, and every year.

:::caution note

Before creating a timed leaderboard, note that timed leaderboards only work for certain kinds of placeholder stats.

You can only make timed leaderboards for stats that are cumulative (adding up over time).

For example, you **can** make a timed leaderboard for total number of kills, total number of votes, etc.<br/>
You **cannot** make a timed leaderboard for things such as a high score.

You can still make a leaderboard for non-cumulative stats, but you should only use `alltime`. The other timed types will not work correctly.

:::

To create a timed leaderboard, you must first follow steps 1 and 2 the same way you would if you were making an all-time leaderboard.
If you've already created an all-time leaderboard, you do not need to follow the steps again.

Once you've added the placeholder to ajLeaderboards, to display timed leaderboards, simply use the `alltime` displays, but replace all `alltime`s with the correct type.

The valid types are
`alltime`
`hourly`
`daily`
`weekly`
`monthly` and
`yearly`

For example, if you wanted to change the hologram example above from alltime to monthly, this is how it would be:
```
&61. &e%ajlb_lb_murdermystery_wins_1_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_1_monthly_value%
&62. &e%ajlb_lb_murdermystery_wins_2_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_2_monthly_value%
&63. &e%ajlb_lb_murdermystery_wins_3_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_3_monthly_value%
&64. &e%ajlb_lb_murdermystery_wins_4_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_4_monthly_value%
&65. &e%ajlb_lb_murdermystery_wins_5_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_5_monthly_value%
&66. &e%ajlb_lb_murdermystery_wins_6_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_6_monthly_value%
&67. &e%ajlb_lb_murdermystery_wins_7_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_7_monthly_value%
&68. &e%ajlb_lb_murdermystery_wins_8_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_8_monthly_value%
&69. &e%ajlb_lb_murdermystery_wins_9_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_9_monthly_value%
&610. &e%ajlb_lb_murdermystery_wins_10_monthly_name% &7- &e%ajlb_lb_murdermystery_wins_10_monthly_value%
```
Notice how the `<type>` in the placeholder was changed from `alltime` to `monthly`
