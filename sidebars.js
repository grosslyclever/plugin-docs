module.exports = {
  ajparkour: [
    {
      type: 'category',
      label: 'ajParkour',
      items: [
        'ajParkour/Home',
        {
          "Setup": [
            'ajParkour/setup/Creating an area',
            'ajParkour/setup/Permissions',
            'ajParkour/setup/placeholders'
          ]
        },
        {
          "Configs": [
            'ajParkour/configs/main',
            'ajParkour/configs/Rewards',
            'ajParkour/configs/Blocks',
            'ajParkour/configs/jumps'
          ]
        },
        'ajParkour/Upgrading from v1 to v2',
        {
          "API": [
            'ajParkour/api/Getting-Started'
          ]
        }
      ]
    }
  ],
  ajleaderboards: [
    {
      type: 'category',
      label: 'ajLeaderboards',
      items: [
        'ajLeaderboards/overview',
        {
          "Setup": [
            'ajLeaderboards/setup/setup',
            'ajLeaderboards/setup/placeholders',
            'ajLeaderboards/setup/permissions',
            'ajLeaderboards/setup/message-placeholders',
            'ajLeaderboards/setup/luckperms-contexts',
            'ajLeaderboards/setup/extras'
          ]
        },
        {
          "Configs": [
            'ajLeaderboards/configs/main',
            'ajLeaderboards/configs/cache_storage'
          ]
        },
        'ajLeaderboards/moving-storage-methods',
        'ajLeaderboards/faq',
        'ajLeaderboards/glossary'
      ]
    }
  ],
  ajqueue: [
    {
      type: 'category',
      label: 'ajQueue',
      items: [
        'ajQueue/overview',
        {
          "Setup": [
            'ajQueue/setup/permissions',
            'ajQueue/setup/placeholders',
            'ajQueue/setup/replacing-server-command',
            'ajQueue/setup/queue-scoreboard'
          ]
        },
        {
          "Configs": [
            'ajQueue/configs/main',
            'ajQueue/configs/spigot'
          ]
        },
        'ajQueue/priority-explanation',
        'ajQueue/faq',
        'ajQueue/api'
      ]
    }
  ],
  ajstartcommands: [
    {
      type: 'category',
      label: 'ajStartCommands',
      items: [
        'ajStartCommands/overview',
        'ajStartCommands/permissions',
        'ajStartCommands/flags'
      ]
    }
  ],
  ajtntrun: [
    {
      type: 'category',
      label: 'ajTNTRun',
      items: [
        'ajTNTRun/overview',
        {
          "Setup": [
            'ajTNTRun/setup/creating-an-arena',
            'ajTNTRun/setup/setting-a-lobby',
            'ajTNTRun/setup/creating-signs',
            'ajTNTRun/setup/arena-requirements',
            'ajTNTRun/setup/commands',
            'ajTNTRun/setup/placeholders'
          ]
        },
        'ajTNTRun/bungee'
      ]
    }
  ]
};
